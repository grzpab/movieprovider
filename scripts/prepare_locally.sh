#!/usr/bin/env bash
set -e

eval $(docker-machine env -u)
docker-compose down

docker-compose build
docker-compose up -d

sleep 5

docker run --rm --net movieprovider_default --link movieprovider_database_1:postgres postgres:latest dropdb -h postgres -U postgres postgres
docker run --rm --net movieprovider_default --link movieprovider_database_1:postgres postgres:latest createdb -h postgres -U postgres postgres

docker-compose run --rm backend python manage.py migrate --noinput
