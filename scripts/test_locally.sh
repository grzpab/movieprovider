#!/usr/bin/env bash
set -e

eval $(docker-machine env -u)

docker-compose up -d database

sleep 5

docker-compose run --rm backend pytest
