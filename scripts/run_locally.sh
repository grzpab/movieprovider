#!/usr/bin/env bash
set -e

eval $(docker-machine env -u)
docker-compose down

docker-compose build
docker-compose up -d database

sleep 5

docker-compose run --rm backend python manage.py migrate --noinput

docker-compose up
