# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework import serializers

from movieprovider.apps.movies.models import MovieModel, ProfileModel, SuggestionModel


class MovieSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = MovieModel
        fields = (
            'id',
            'title',
            'link',
            'imdb_rating',
            'genres',
        )


class ProfileSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ProfileModel
        fields = (
            'id',
            'minimal_imdb_rating',
            'genre_whitelist',
            'genre_blacklist',
        )


class SuggestionSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    movie = MovieSerializer()
    profile = ProfileSerializer()

    class Meta(object):
        model = SuggestionModel
        fields = (
            'id',
            'created_at',
            'movie',
            'profile',
            'status',
        )
