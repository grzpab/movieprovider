# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import logging

from django.db.models import Q
import feedparser

from movieprovider.apps.movies.models import MovieModel


logger = logging.getLogger(__name__)


class FeedService(object):
    URL = 'https://yourbittorrent.com/movies/rss.xml'
    STARTSWITH = [
        '(', '-', '720p', '1080p', 'dvd', 'bluray', 'aac', 'x265', 'x264', 'mp3', 'hc', 'evo', 'ac3',
    ]

    WITHIN = [
        'rip', 'www', 'subs', 'bit',
    ]

    ENDSWITH = [
        ')', 'cd', 'mb', 'gb',
    ]

    FIRST_YEAR_OF_MODERN_CINEMATOGRAPHY = 1975

    @classmethod
    def _is_negligible(cls, word):
        word = word.lower()

        for startswith in cls.STARTSWITH:
            if word.startswith(startswith):
                return True

        for within in cls.WITHIN:
            if within in word:
                return True

        for endswith in cls.ENDSWITH:
            if word.endswith(endswith):
                return True

        try:
            if int(word) > cls.FIRST_YEAR_OF_MODERN_CINEMATOGRAPHY:
                return True

        except ValueError:
            pass

        return False

    @classmethod
    def get_movies(cls):
        feed = feedparser.parse(cls.URL)

        for entry in feed['entries']:
            link = entry['link']

            words = entry['title'].encode('ascii', 'ignore').split(' ')
            words = filter(lambda w: not cls._is_negligible(word=w), words)
            title = ' '.join(words)

            if MovieModel.objects.filter(Q(link=link) | Q(title=title)).exists():
                logger.debug('MovieModel with title=%s or link=%s already exists', title, link)
                continue

            movie = MovieModel.objects.create(link=link, title=title)

            logger.debug('MovieModel #%d (%s) has been created', movie.id, movie.title)
