# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework.routers import DefaultRouter

from movieprovider.apps.movies.resources import SuggestionViewSet

router = DefaultRouter()
router.register(r'suggestions', SuggestionViewSet)

urlpatterns = router.urls
