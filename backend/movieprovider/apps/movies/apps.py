# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig


class MoviesAppConfig(AppConfig):
    name = 'movieprovider.apps.movies'

    def ready(self):
        import movieprovider.apps.movies.signals
