# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


from django.contrib import admin

from movieprovider.apps.movies.models import MovieModel, ProfileModel, SuggestionModel

admin.site.register(MovieModel)
admin.site.register(ProfileModel)
admin.site.register(SuggestionModel)
