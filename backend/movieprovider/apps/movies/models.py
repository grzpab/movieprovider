# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.postgres.fields import ArrayField
from django.db import models
from enumfields import EnumIntegerField

from movieprovider.apps.movies.enums import SuggestionStatus


class MovieModel(models.Model):
    title = models.TextField(blank=False, null=False, unique=True)
    link = models.TextField(blank=False, null=False, unique=True)
    imdb_rating = models.DecimalField(max_digits=4, decimal_places=2, null=True)
    genres = ArrayField(models.TextField(), null=True)


class ProfileModel(models.Model):
    minimal_imdb_rating = models.DecimalField(max_digits=4, decimal_places=2)
    genre_whitelist = ArrayField(models.TextField(), null=True)
    genre_blacklist = ArrayField(models.TextField(), null=True)


class SuggestionModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    movie = models.ForeignKey('movies.MovieModel', related_name='+', null=False)
    profile = models.ForeignKey('movies.ProfileModel', related_name='suggestions', null=False)
    status = EnumIntegerField(SuggestionStatus, default=SuggestionStatus.UNDECIDED, null=False)

    class Meta:
        ordering = ('-created_at', 'id', )
