# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from enumfields import IntEnum


class SuggestionStatus(IntEnum):
    UNDECIDED = 1
    DISMISSED = 2
    ACCEPTED = 3
