# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django_filters import FilterSet
from rest_framework import status
from rest_framework import viewsets, filters
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from movieprovider.apps.movies.enums import SuggestionStatus
from movieprovider.apps.movies.models import SuggestionModel
from movieprovider.apps.movies.serializers import SuggestionSerializer


class SuggestionFilter(FilterSet):
    class Meta(object):
        model = SuggestionModel
        fields = {
            'profile__id': ('exact', )
        }


class SuggestionViewSet(viewsets.ReadOnlyModelViewSet):
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter,)
    filter_class = SuggestionFilter
    serializer_class = SuggestionSerializer
    queryset = SuggestionModel.objects.all()

    @detail_route(methods=['post'])
    def accept(self, request, pk=None):
        suggestion = self.get_object()
        suggestion.status = SuggestionStatus.ACCEPTED
        suggestion.save()

        serializer = self.get_serializer_class()(instance=suggestion)

        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['post'])
    def dismiss(self, request, pk=None):
        suggestion = self.get_object()
        suggestion.status = SuggestionStatus.DISMISSED
        suggestion.save()

        serializer = self.get_serializer_class()(instance=suggestion)

        return Response(serializer.data, status=status.HTTP_200_OK)
