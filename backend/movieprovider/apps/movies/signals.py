# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


from decimal import Decimal
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
import imdb

from movieprovider.apps.movies.models import MovieModel, ProfileModel, SuggestionModel


@receiver(post_save, sender=MovieModel)
def movie_model_post_creation(sender, instance, created, **kwargs):
    if not created or ProfileModel.objects.count() == 0:
        return

    if not instance.imdb_rating and not instance.genres:
        ia = imdb.IMDb()
        results = ia.search_movie(instance.title)

        if not results:
            return

        result = results[0]
        ia.update(result)

        imdb_rating = result.get('rating', '0')
        genres = result.get('genres', None)

        instance.imdb_rating = Decimal(imdb_rating)
        instance.genres = genres

        instance.save()  # will call the current function once again, but will stop at the first instruction

    genre_set = set(instance.genres)

    for profile in ProfileModel.objects.all():
        if instance.imdb_rating < profile.minimal_imdb_rating:
            continue

        if profile.genre_whitelist and genre_set.isdisjoint(set(profile.genre_whitelist)):
            # if no whitelist present, then pass it through
            # if there is one, there must be at least one match
            continue

        if profile.genre_blacklist and not genre_set.isdisjoint(set(profile.genre_blacklist)):
            # if no blacklist present, then pass it through
            # if there is one, then the sets must be disjoint
            continue

        SuggestionModel.objects.create(
            movie=instance,
            profile=profile
        )
