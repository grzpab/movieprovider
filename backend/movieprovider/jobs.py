# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from movieprovider.apps.movies.services import FeedService
from movieprovider.celery import celery_app


@celery_app.task
def create_movies_from_rss_job():
    FeedService.get_movies()
