# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from decimal import Decimal
from django.test import TestCase
import vcr

from movieprovider.apps.movies.models import MovieModel, ProfileModel, SuggestionModel
from movieprovider.apps.movies.services import FeedService


class TestFeeds(TestCase):
    def test_querying(self):
        '''
        TODO: imdbpy is using urllib internally, vcrpy is supporting urllib2 and urllib3, as do all the major cassette
        libraries - this means that no requests to imdb will be persisted.
        '''
        with vcr.use_cassette('movieprovider/tests/cassettes/test_feed.yml'):
            FeedService.get_movies()

        # feed has 30 entries, but 5 are duplicated
        self.assertEqual(MovieModel.objects.count(), 25)

        with vcr.use_cassette('movieprovider/tests/cassettes/test_feed.yml'):
            FeedService.get_movies()

        # no new entries added
        self.assertEqual(MovieModel.objects.count(), 25)

    def test_movie_imdb_data_fetching(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5'),
            genre_whitelist=[],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
        )

        movie = MovieModel.objects.get(id=movie.id)

        self.assertEqual(movie.imdb_rating, Decimal('8.7000'))
        self.assertEqual(movie.genres, ['Action', 'Sci-Fi'])

        self.assertEqual(SuggestionModel.objects.count(), 1)

    def test_suggestion_with_imdb_rating(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.75'),
            genre_whitelist=[],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=[],
        )

        self.assertEqual(SuggestionModel.objects.count(), 0)

    def test_suggestion_whitelist_positive(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=['A', 'B', 'C'],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['A'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 1)

    def test_suggestion_whitelist_negative(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=['A', 'B', 'C'],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['Z'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 0)

    def test_suggestion_blacklist_positive(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=[],
            genre_blacklist=['D', 'E', 'F'],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['Z'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 1)

    def test_suggestion_blacklist_negative(self):
        ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=[],
            genre_blacklist=['D', 'E', 'F'],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['D'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 0)
