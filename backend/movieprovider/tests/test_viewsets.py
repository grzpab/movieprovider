# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from decimal import Decimal

from rest_framework.test import APITestCase

from movieprovider.apps.movies.enums import SuggestionStatus
from movieprovider.apps.movies.models import ProfileModel, MovieModel, SuggestionModel


class TestViewSets(APITestCase):
    def test_getting_suggestion(self):
        profile = ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=['A', 'B', 'C'],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['A'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 1)

        suggestion = SuggestionModel.objects.first()

        response = self.client.get('/movies/suggestions/{}/'.format(suggestion.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {
            'id': suggestion.id,
            'created_at': suggestion.created_at.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            'movie': {
                'id': movie.id,
                'title': 'The Matrix',
                'link': 'http://example.com',
                'imdb_rating': '5.70',
                'genres': ['A', ],
            },
            'profile': {
                'id': profile.id,
                'minimal_imdb_rating': '5.00',
                'genre_whitelist': ['A', 'B', 'C', ],
                'genre_blacklist': [],
            },
            'status': 'UNDECIDED',
        })

    def test_getting_suggestions_by_profile(self):
        profile_1 = ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=['A', 'B', 'C'],
            genre_blacklist=[],
        )

        profile_2 = ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('3.00'),
            genre_whitelist=[],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['A'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 2)
        self.assertEqual(SuggestionModel.objects.filter(profile=profile_1).count(), 1)
        self.assertEqual(SuggestionModel.objects.filter(profile=profile_2).count(), 1)

        suggestion_1 = SuggestionModel.objects.filter(profile=profile_1).first()
        suggestion_2 = SuggestionModel.objects.filter(profile=profile_2).first()

        response = self.client.get('/movies/suggestions/', data={
            'profile__id': profile_1.id,
        })
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['count'], 1)
        self.assertEqual(response_json['results'][0]['id'], suggestion_1.id)

        response = self.client.get('/movies/suggestions/', data={
            'profile__id': profile_2.id,
        })
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['count'], 1)
        self.assertEqual(response_json['results'][0]['id'], suggestion_2.id)

    def test_acceptance_and_dismissals(self):
        profile = ProfileModel.objects.create(
            minimal_imdb_rating=Decimal('5.00'),
            genre_whitelist=['A', 'B', 'C'],
            genre_blacklist=[],
        )

        movie = MovieModel.objects.create(
            title='The Matrix',
            link='http://example.com',
            imdb_rating=Decimal('5.70'),
            genres=['A'],
        )

        self.assertEqual(SuggestionModel.objects.count(), 1)

        suggestion = SuggestionModel.objects.first()
        self.assertEqual(suggestion.status, SuggestionStatus.UNDECIDED)

        response = self.client.post('/movies/suggestions/{}/accept/'.format(suggestion.id))
        self.assertEqual(response.status_code, 200)

        suggestion = SuggestionModel.objects.get(id=suggestion.id)
        self.assertEqual(suggestion.status, SuggestionStatus.ACCEPTED)

        response = self.client.post('/movies/suggestions/{}/dismiss/'.format(suggestion.id))
        self.assertEqual(response.status_code, 200)

        suggestion = SuggestionModel.objects.get(id=suggestion.id)
        self.assertEqual(suggestion.status, SuggestionStatus.DISMISSED)
