# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'movieprovider.settings')

celery_app = Celery('movieprovider')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()
