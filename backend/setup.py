# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from setuptools import setup
from setuptools.sandbox import DirectorySandbox


def violation(operation, *args, **_):
    pass

DirectorySandbox._violation = violation

setup(
    name='movieprovider',
    version='0.0.1',
    description='A service that provides newest movie information based on user criteria',
    url='http://bitbucket.org/grzpab/movieprovider',
    author='Gregory P. Pabian',
    author_email='grzpab@gmail.com',
    license='MIT',
    packages=[
        'movieprovider',
    ],
    install_requires=[
        'Django==1.10.5',
        'django-enumfields==0.8.2',
        'django-filter==0.13.0',
        'django-oauth-toolkit==0.10.0',
        'djangorestframework==3.4.1',
        'enum34==1.1.6',
        'fake-factory==0.6.0',
        'ipaddress==1.0.16',
        'Markdown==2.6.6',
        'oauthlib==1.0.3',
        'python-dateutil==2.5.3',
        'pytz==2016.6.1',
        'six==1.10.0',
        'gunicorn==19.6.0',
        'psycopg2==2.6.2',
        'vcrpy==1.10.5',
        'feedparser==5.2.1',
        'SQLAlchemy==1.1.6',
        'drf-enum-field',
        'imdbpy',
        'pytest-django==3.1.2',
        'django-crontab==0.7.1',
        'celery==4.0.2',
        'django-celery-beat==1.0.1',
        'python-memcached==1.58',
    ],
    dependency_links=[
        'git+http://github.com/seebass/drf-enum-field.git@738ca65#egg=drf-enum-field',
        'https://bitbucket.org/alberanid/imdbpy/get/5.1.zip#egg=imdbpy',
    ],
    zip_safe=False,
)
