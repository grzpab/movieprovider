# MovieProvider
A simple service for proposing the most popular movies.

## Requirements
- Docker 1.13+

## Technological Stack
- Python 2.7
- Setuptools
- Django 1.10
- Django REST Framework 3.5
- pytest
- PostgreSQL 9.6
- Celery 4
- RabbitMQ
- Memcached

## [Optional] Download all dependencies to be accessible for IDEs
```
sudo apt-get install -y build-essential libxml2 libxslt-dev

virtualenv venv
source venv/bin/activate
cd backend
python setup.py install
```

## Scripts
- `./scripts/prepare_locally.sh` for setup the project
- `./scripts/test_locally.sh` to run all test cases
- `./scripts/run_locally.sh` to run the entire microservice

## Scheduled tasks
The scheduled task that fetches an RSS feed with movies every 12 hours is managed by Celery/RabbitMQ/Memcached stack, which is a more elegant and error-proof solution than using crontab entries.
The downloaded data are parsed and based on the content, new movie entries are added to the database.
Duplicate entries are thrown away.
For each newly added movie, an algorithm checks it against the defined profiles and supplies the end user with suggestions.

## JSON API
No authentication is required for public endpoints since no models can be created by the end-users.

- `GET /movies/suggestions/` to get an entire, paginated list of suggestions
- `GET /movies/suggestions/<ID>/` to get a single suggestion
- `GET /movies/suggestions/?profile__id=<ID>` to get suggestions for a particular profile ID
- `POST /movies/suggestions/<ID>/accept/` to accept a suggestion (returns 200 or 404)
- `POST /movies/suggestions/<ID>/dismiss/` to dismiss a suggestion (return 200 or 404)

An exemplary response body for a single suggestion looks like:
```
"id": 1,
"created_at": "2017-03-05T10:10:00.123456Z",
"movie": {
    "id": 2,
    "title": "The Matrix",
    "link": "http://example.com",
    "imdb_rating": "5.70",
    "genres": ["A"]
},
"profile": {
    "id": 3,
    "minimal_imdb_rating": "5.00",
    "genre_whitelist": ["A", "B", "C"],
    "genre_blacklist": []
},
"status": "UNDECIDED"
```
 
